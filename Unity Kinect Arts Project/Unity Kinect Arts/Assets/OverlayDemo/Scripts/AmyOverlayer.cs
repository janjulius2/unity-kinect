﻿using System;
using UnityEngine;
using System.Collections;

public class AmyOverlayer : MonoBehaviour
{
    //	public Vector3 TopLeft;
    //	public Vector3 TopRight;
    //	public Vector3 BottomRight;
    //	public Vector3 BottomLeft;

    public GUITexture backgroundImage;
    public KinectWrapper.NuiSkeletonPositionIndex TrackedJoint = KinectWrapper.NuiSkeletonPositionIndex.HandRight;
    public KinectWrapper.NuiSkeletonPositionIndex TrackedJointL = KinectWrapper.NuiSkeletonPositionIndex.HandLeft;
    public bool EnableRightHand = true;
    public GameObject OverlayObject;
    public bool EnableLeftHand = false;
    public GameObject OverlayObjectL;
    public float smoothFactor = 5f;

    public GUIText debugText;

    private float distanceToCamera = 10f;


    public enum DrawingMethod
    {
        Nothing,
        Bubbles,
        Signs,
    };

    public enum InteractionMethod
    {
        Dissapear,
        Nothing,
        Move,
        
    };

    [SerializeField] public DrawingMethod rDrawing = DrawingMethod.Bubbles;
    [SerializeField] public InteractionMethod rInteract = InteractionMethod.Dissapear;
    
    [SerializeField] public DrawingMethod lDrawing = DrawingMethod.Bubbles;
    [SerializeField] public InteractionMethod lInteract = InteractionMethod.Dissapear;

    public GameObject bgObject;


    void Start()
    {
        bgObject = FindObjectOfType<BackgroundObject>().gameObject;
        //foreach (var g in bgObject.GetComponentsInChildren<CollisionInactivity>())
        //{
        //    print(g);
        //}
        if (rDrawing == DrawingMethod.Bubbles)
        {
            OverlayObject.GetComponent<BubbleSpawner>().enabled = true;
        }
        else if (rDrawing == DrawingMethod.Signs)
        {
            OverlayObject.GetComponent<SignsSpawner>().enabled = true;
        }

        if (rInteract == InteractionMethod.Dissapear)
        {
            OverlayObject.tag = "Dissapear";
        }
        else if (rInteract == InteractionMethod.Move)
        {
            OverlayObject.tag = "Interact";
        }

        if (lDrawing == DrawingMethod.Bubbles)
        {
            OverlayObjectL.GetComponent<BubbleSpawner>().enabled = true;
        }
        else if (lDrawing == DrawingMethod.Signs)
        {
            OverlayObjectL.GetComponent<SignsSpawner>().enabled = true;
        }

        if (lInteract == InteractionMethod.Dissapear)
        {
            OverlayObjectL.tag = "Dissapear";
        }
        else if (lInteract == InteractionMethod.Move)
        {
            OverlayObjectL.tag = "Interact";
        }

        if (OverlayObject)
        {
            distanceToCamera = (OverlayObject.transform.position - Camera.main.transform.position).magnitude;
        }
        if (OverlayObjectL)
        {
            distanceToCamera = (OverlayObjectL.transform.position - Camera.main.transform.position).magnitude;
        }
    }

    void Update()
    {
        KinectManager manager = KinectManager.Instance;

        if (manager && manager.IsInitialized())
        {
            //backgroundImage.renderer.material.mainTexture = manager.GetUsersClrTex();
            if (backgroundImage && (backgroundImage.texture == null))
            {
                backgroundImage.texture = manager.GetUsersClrTex();
            }

            //			Vector3 vRight = BottomRight - BottomLeft;
            //			Vector3 vUp = TopLeft - BottomLeft;

            int iJointIndex = (int)TrackedJoint;
            int iJointIndexL = (int)TrackedJointL;

            if (manager.IsUserDetected())
            {
                uint userId = manager.GetPlayer1ID();

                if (manager.IsJointTracked(userId, iJointIndex))
                {
                    Vector3 posJoint = manager.GetRawSkeletonJointPos(userId, iJointIndex);

                    if (posJoint != Vector3.zero)
                    {
                        // 3d position to depth
                        Vector2 posDepth = manager.GetDepthMapPosForJointPos(posJoint);

                        // depth pos to color pos
                        Vector2 posColor = manager.GetColorMapPosForDepthPos(posDepth);

                        float scaleX = (float) posColor.x / KinectWrapper.Constants.ColorImageWidth;
                        float scaleY = 1.0f - (float) posColor.y / KinectWrapper.Constants.ColorImageHeight;

                        //						Vector3 localPos = new Vector3(scaleX * 10f - 5f, 0f, scaleY * 10f - 5f); // 5f is 1/2 of 10f - size of the plane
                        //						Vector3 vPosOverlay = backgroundImage.transform.TransformPoint(localPos);
                        //Vector3 vPosOverlay = BottomLeft + ((vRight * scaleX) + (vUp * scaleY));

                        if (debugText)
                        {
                            debugText.GetComponent<GUIText>().text =
                                "Tracked user ID: " + userId; // new Vector2(scaleX, scaleY).ToString();
                        }

                        if (OverlayObject)
                        {
                            Vector3 vPosOverlay =
                                Camera.main.ViewportToWorldPoint(new Vector3(scaleX, scaleY, distanceToCamera));
                            OverlayObject.transform.position = Vector3.Lerp(OverlayObject.transform.position,
                                vPosOverlay, smoothFactor * Time.deltaTime);
                        }
                        
                    }
                }

                if (manager.IsJointTracked(userId, iJointIndexL))
                {
                    Vector3 posJoint = manager.GetRawSkeletonJointPos(userId, iJointIndexL);

                    if (posJoint != Vector3.zero)
                    {
                        // 3d position to depth
                        Vector2 posDepth = manager.GetDepthMapPosForJointPos(posJoint);

                        // depth pos to color pos
                        Vector2 posColor = manager.GetColorMapPosForDepthPos(posDepth);

                        float scaleX = (float)posColor.x / KinectWrapper.Constants.ColorImageWidth;
                        float scaleY = 1.0f - (float)posColor.y / KinectWrapper.Constants.ColorImageHeight;

                        //						Vector3 localPos = new Vector3(scaleX * 10f - 5f, 0f, scaleY * 10f - 5f); // 5f is 1/2 of 10f - size of the plane
                        //						Vector3 vPosOverlay = backgroundImage.transform.TransformPoint(localPos);
                        //Vector3 vPosOverlay = BottomLeft + ((vRight * scaleX) + (vUp * scaleY));

                        if (debugText)
                        {
                            debugText.GetComponent<GUIText>().text =
                                "Tracked user ID: " + userId; // new Vector2(scaleX, scaleY).ToString();
                        }

                        if (OverlayObjectL)
                        {
                            Vector3 vPosOverlay =
                                Camera.main.ViewportToWorldPoint(new Vector3(scaleX, scaleY, distanceToCamera));
                            OverlayObjectL.transform.position = Vector3.Lerp(OverlayObjectL.transform.position,
                                vPosOverlay, smoothFactor * Time.deltaTime);
                        }

                    }
                }
                

            }
            else
            {
                OverlayObject.transform.position = new Vector3(20, 0, OverlayObject.transform.position.z);
                OverlayObjectL.transform.position = new Vector3(20, 0, OverlayObjectL.transform.position.z);
            }

        }
    }
}
