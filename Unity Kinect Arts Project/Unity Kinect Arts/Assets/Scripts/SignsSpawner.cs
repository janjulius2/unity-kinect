﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignsSpawner : Spawner {

    public void Start()
    {
        if (isRightHand)
        {
            print("signs Right hand: set to: " + FindObjectOfType<GameManager>().GetRDrawingMethod() + " equation: " +(FindObjectOfType<GameManager>().GetRDrawingMethod() == AmyOverlayer.DrawingMethod.Signs));
            this.enabled = FindObjectOfType<GameManager>().GetRDrawingMethod() == AmyOverlayer.DrawingMethod.Signs;
        }
        else
        {
            this.enabled = FindObjectOfType<GameManager>().GetLDrawingMethod() == AmyOverlayer.DrawingMethod.Signs;
        }
        if (enabled)
        {
            InvokeRepeating("InstantiateRainObject", Random.Range(0.001f, 0.002f),
                Random.Range(minInvokeRepeating, maxInvokeRepeating));
        }
    }

}
