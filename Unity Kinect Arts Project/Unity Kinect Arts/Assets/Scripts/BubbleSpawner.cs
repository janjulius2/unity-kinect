﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleSpawner : Spawner
{
    public void Start()
    {
        if (isRightHand)
        {
            this.enabled = FindObjectOfType<GameManager>().GetRDrawingMethod() == AmyOverlayer.DrawingMethod.Bubbles;
        }
        else
        {
            this.enabled = FindObjectOfType<GameManager>().GetLDrawingMethod() == AmyOverlayer.DrawingMethod.Bubbles;
        }
        if (enabled)
        {
            InvokeRepeating("InstantiateRainObject", Random.Range(0.001f, 0.002f),
                Random.Range(minInvokeRepeating, maxInvokeRepeating));
        }
    }
}
