﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundObject : MonoBehaviour
{
    public List<Vector3> ObjectStartPositions = new List<Vector3>();

    public List<GameObject> MyObjects = new List<GameObject>();

    void Start()
    {
        for (var index = 0; index < GetComponentsInChildren<CollisionInactivity>().Length; index++)
        {
            ObjectStartPositions.Add(GetComponentsInChildren<CollisionInactivity>()[index].gameObject.transform.position); 
            MyObjects.Add(GetComponentsInChildren<CollisionInactivity>()[index].gameObject);
        }

    }

    public void ResetPieces()
    {
        for (int i = 0; i < GetComponentsInChildren<CollisionInactivity>().Length; i++)
        {
            GetComponentsInChildren<CollisionInactivity>()[i].gameObject.transform.position = ObjectStartPositions
                [i];
        }
    }

    public void SetPiecesInverse(bool inverse)
    {
        foreach (var obj in MyObjects)
        {
            obj.GetComponent<CollisionInactivity>().SetInverse(inverse);
        }
    }

    public void SetPiecesTag(String n)
    {
        foreach (var obj in MyObjects)
        {
            obj.tag = n;
        }
    }
}
