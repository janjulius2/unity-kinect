﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class Rain : MonoBehaviour {
    
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "rain")
        {
            Physics.IgnoreCollision(collision.collider, gameObject.GetComponent<Collider>());
        }
    }

    void Start()
    {
        Destroy(gameObject, 3f);
    }
}
