﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleRemover : MonoBehaviour {

    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "puzzlepiece")
        {
            other.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    public void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "puzzlepiece")
        {
            other.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
    }
}
