﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rainscript : MonoBehaviour
{

    public int maxRain = 5000;
    private int currentRain = 0;
    public int intesity = 100;
    public int minX = 0;
    public int maxX = 2;

    public GameObject RainPrefab;

    void Start()
    {
        InvokeRepeating("InstantiateRainObject", Random.Range(0.001f, 0.002f), Random.Range(0.001f, 0.002f));
    }

    void InstantiateRainObject()
    {
        currentRain++;
        GameObject LastInstantiatedObject =
            Instantiate(RainPrefab,
                new Vector3(Random.Range(minX, maxX),
                    0,
                    0
                ),
                Quaternion.identity);
    }
}
