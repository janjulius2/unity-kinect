﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{

    public float TimeBeforeDestroying = 2f;

    public void Start()
    {
        Destroy(gameObject, TimeBeforeDestroying);
    }
}
