﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class CollisionInactivity : MonoBehaviour
{
    public GameObject collisionObject;

    public bool dissapearOnTouch = true;
    private bool inverse;
    private bool spaceBehaviour = false;

    public void Start()
    {
        inverse = FindObjectOfType<GameManager>().getInverse();
        print("inverse set to: " + inverse);
        if (inverse)
        {
            this.GetComponent<SpriteRenderer>().enabled = false;
        }
        if (spaceBehaviour)
        {
            this.GetComponent<Rigidbody>().useGravity = true;
            //this.GetComponent<Rigidbody>().mass = 0;
            this.GetComponent<Collider>().isTrigger = false;
            Physics.IgnoreLayerCollision(8, 8);
        }
        else
        {
            if (this.GetComponent<Rigidbody>().useGravity)
                this.GetComponent<Rigidbody>().useGravity = false;
        }
    }

    public void SetInverse(bool b)
    {
        inverse = b;
        this.GetComponent<SpriteRenderer>().enabled = !inverse;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (dissapearOnTouch && collision.gameObject.tag == "Dissapear")
        {
            this.gameObject.GetComponent<SpriteRenderer>().enabled = inverse;
        }
        if (collision.gameObject.tag == "puzzlepiece" || collision.gameObject.tag == "Dissapear")
        {
            Physics.IgnoreCollision(collision.collider, gameObject.GetComponent<Collider>());
        }
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (dissapearOnTouch && collision.gameObject.tag == "Dissapear")
        {
            this.gameObject.GetComponent<SpriteRenderer>().enabled = inverse;
        }
        /*if (collision.gameObject.tag == "puzzlepiece" || collision.gameObject.tag == "Dissapear")
        {
            Physics.IgnoreCollision(collision.GetComponent<Collider>(), gameObject.GetComponent<Collider>());
        }*/

    }

    public void OnCollisionExit(Collision collision)
    {
        if (dissapearOnTouch && collision.gameObject.tag == "Dissapear")
        {
            print("renable");
            this.gameObject.GetComponent<SpriteRenderer>().enabled = !inverse;
        }
        if (collision.gameObject.tag == "puzzlepiece" || collision.gameObject.tag == "Dissapear")
        {
            Physics.IgnoreCollision(collision.collider, gameObject.GetComponent<Collider>());
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if(dissapearOnTouch && other.gameObject.tag == "Dissapear")
        {
            this.gameObject.GetComponent<SpriteRenderer>().enabled = !inverse;
        }
    }
}
