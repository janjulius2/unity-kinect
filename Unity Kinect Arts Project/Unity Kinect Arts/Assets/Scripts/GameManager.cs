﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    private AmyOverlayer aO;
    private bool inverse = false;
    private AmyOverlayer.DrawingMethod LDrawingMethod;
    private AmyOverlayer.DrawingMethod RDrawingMethod;
    private AmyOverlayer.InteractionMethod LInteractionMethod;
    private AmyOverlayer.InteractionMethod RInteractionMethod;

    public Text inverseText;
    public Dropdown LDHandDropdown;
    public Dropdown RDHandDropdown;
    public Dropdown LIHandDropdown;
    public Dropdown RIHandDropdown;

    public void Start()
    {
        DontDestroyOnLoad(this.gameObject);


        if (FindObjectOfType<AmyOverlayer>() == null)
        {
            UpdateUI();
            LoadUI();
        }
    }

    public void SetMethods(int method)
    {
        switch (method)
        {
            case 0:
                LDrawingMethod = Enum.GetValues(typeof(AmyOverlayer.DrawingMethod)).Cast<AmyOverlayer.DrawingMethod>().ToList()[LDHandDropdown.value];
                break;
            case 1:
                RDrawingMethod = Enum.GetValues(typeof(AmyOverlayer.DrawingMethod)).Cast<AmyOverlayer.DrawingMethod>().ToList()[RDHandDropdown.value];
                //SetMethod2(RDrawingMethod, RDHandDropdown.value);
                break;
            case 2:
                LInteractionMethod = Enum.GetValues(typeof(AmyOverlayer.InteractionMethod)).Cast<AmyOverlayer.InteractionMethod>().ToList()[LIHandDropdown.value];
                //SetMethod(LInteractionMethod, LIHandDropdown.value);
                break;
            case 3:
                RInteractionMethod = Enum.GetValues(typeof(AmyOverlayer.InteractionMethod)).Cast<AmyOverlayer.InteractionMethod>().ToList()[RIHandDropdown.value];
                //SetMethod2(RInteractionMethod, RIHandDropdown.value);
                break;
        }
    }

    private void SetMethod(AmyOverlayer.DrawingMethod a, int value)
    {
        a = Enum.GetValues(typeof(AmyOverlayer.DrawingMethod)).Cast<AmyOverlayer.DrawingMethod>().ToList()[value];
        print(a);
    }
    private void SetMethod2(AmyOverlayer.DrawingMethod a, int value)
    {
        a = Enum.GetValues(typeof(AmyOverlayer.DrawingMethod)).Cast<AmyOverlayer.DrawingMethod>().ToList()[value];
        print(a);
    }

    private void SetMethod(AmyOverlayer.InteractionMethod a, int value)
    {
        a = Enum.GetValues(typeof(AmyOverlayer.InteractionMethod)).Cast<AmyOverlayer.InteractionMethod>().ToList()[value];
        print(a);
    }
    private void SetMethod2(AmyOverlayer.InteractionMethod a, int value)
    {
        a = Enum.GetValues(typeof(AmyOverlayer.InteractionMethod)).Cast<AmyOverlayer.InteractionMethod>().ToList()[value];
        print(a);
    }

    public void Inverse()
    {
        inverse = !inverse;
        UpdateUI();
    }

    public void LoadUI()
    {
        RDHandDropdown.ClearOptions();
        RIHandDropdown.ClearOptions();
        LIHandDropdown.ClearOptions();
        LDHandDropdown.ClearOptions();
        List<Dropdown.OptionData> list1 = new List<Dropdown.OptionData>();
        List<Dropdown.OptionData> list2 = new List<Dropdown.OptionData>();
        
        foreach(AmyOverlayer.DrawingMethod dm in Enum.GetValues(typeof(AmyOverlayer.DrawingMethod)))
        {
            list1.Add(new Dropdown.OptionData(dm.ToString()));
        }
        RDHandDropdown.AddOptions(list1);
        LDHandDropdown.AddOptions(list1);

        foreach (AmyOverlayer.InteractionMethod dm in Enum.GetValues(typeof(AmyOverlayer.InteractionMethod)))
        {
            list2.Add(new Dropdown.OptionData(dm.ToString()));
        }
        RIHandDropdown.AddOptions(list2);
        LIHandDropdown.AddOptions(list2);
    }

    /// <summary>
    /// im lazy
    /// </summary>
    public void UpdateUI()
    {
        inverseText.text = inverse
            ? "The pieces will now appear"
            : "The pieces will now dissapear";
    }
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    public void Setup()
    {
        SceneManager.LoadScene(1);

        print("setting up...");
        var aO = FindObjectOfType<AmyOverlayer>();
        
        print(aO.bgObject);
        print("all pieces set to: "+ inverse);
        aO.bgObject.GetComponent<BackgroundObject>().SetPiecesInverse(inverse);
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        
        Debug.Log("Level Loaded");
        Debug.Log(scene.name);
        Debug.Log(mode);
    }

    public bool getInverse()
    {
        return inverse;
    }


    IEnumerator CheckBool()
    {
        print("waiting for ao object to load");
        yield return new WaitUntil(() => aO.bgObject != null);
        print("Object has loaded");
    }

    public AmyOverlayer.DrawingMethod GetRDrawingMethod()
    {
        return RDrawingMethod;
    }

    public AmyOverlayer.DrawingMethod GetLDrawingMethod()
    {
        return LDrawingMethod;
    }

    public AmyOverlayer.InteractionMethod GetRInteractionMethod()
    {
        return RInteractionMethod;
    }

    public AmyOverlayer.InteractionMethod GetLInteractionMethod()
    {
        return LInteractionMethod;
    }
}
