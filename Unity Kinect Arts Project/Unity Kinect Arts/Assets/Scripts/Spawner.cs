﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public int objectsSpawned = 0;

    public GameObject[] objectPrefabs;

    public float minInvokeRepeating = 0.05f, maxInvokeRepeating = 0.07f;

    public float objectMinSize = 0.025f, objectMaxSize = 0.05f;

    public bool isRightHand;
    


    void Start()
    {
        print("starting spawning");
        InvokeRepeating("InstantiateRainObject", Random.Range(0.001f, 0.002f),
            Random.Range(minInvokeRepeating, maxInvokeRepeating));
    }

    void InstantiateRainObject()
    {
        objectsSpawned++;
        Vector3 nPos = new Vector3(Random.Range(transform.position.x + 2, transform.position.x - 2),
            Random.Range(transform.position.y + 1, transform.position.y - 1), transform.position.z);
        GameObject LastObject = Instantiate(objectPrefabs[Random.Range(0, objectPrefabs.Length)], nPos, Quaternion.identity);
        float rV = Random.Range(objectMinSize, objectMaxSize);
        Vector3 nSca = new Vector3(rV, rV, rV);
        LastObject.transform.localScale = nSca;
    }
}
